package com.sda.cats.service;

import com.sda.cats.model.Cat;
import com.sda.cats.model.Colour;
import com.sda.cats.repository.CatRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(value = "cats.generate", havingValue = "true") // w configu ustawiamy warunek czy wstawać
public class CatGenerator implements InitializingBean {

    private final CatRepository catRepository;

    @Autowired
    public CatGenerator(CatRepository catRepository) {
        this.catRepository = catRepository;
    }


    @Override
    public void afterPropertiesSet() throws Exception {

        Cat c1 = Cat.builder().age(12).name("kicia").colour(Colour.PINK).hasTail(true).build();
        Cat c2 = Cat.builder().age(32).name("pusia").colour(Colour.BLUE).hasTail(false).build();

        catRepository.save(c1);
        catRepository.save(c2);
    }
}
