package com.sda.cats.model;

import lombok.Data;

@Data
public class CreateCatRequest {

    private String name;
    private Integer age;
    private Boolean hasTail;
    private Colour colour;

}
