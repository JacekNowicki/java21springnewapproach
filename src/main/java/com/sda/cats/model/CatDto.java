package com.sda.cats.model;

import lombok.*;
import org.springframework.stereotype.Service;

@Data
@Builder
public class CatDto {

    private Long id;

    private String name;
    private Integer age;
    private Boolean hasTail;
    private Colour colour;


}
