package com.sda.cats.model;

import lombok.*;

import javax.persistence.*;

@Entity(name = "cats")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Cat {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private Integer age;
    private Boolean hasTail;

    @Enumerated(EnumType.STRING)
    private Colour colour;


}
